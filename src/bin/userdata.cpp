#include "userdata.hpp"

#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <KMessageBox>
#include <QApplication>
#include <QWidget>

UserData::UserData(QWidget *parent)
    : QWidget(parent){
    clientId = getCsproductId();
    limitVer = "limit.30days";
    m_networkManager = new QNetworkAccessManager(this);
}
QString UserData::getCsproductId()
{
    QProcess p;
    p.setReadChannel(QProcess::StandardOutput);
    p.setProcessChannelMode(QProcess::MergedChannels);
    QStringList arg;
    arg << "csproduct" << "get"<<"uuid";
    p.start("wmic", arg);
    p.waitForFinished();
    QString result = p.readAllStandardOutput().simplified().trimmed().toUpper();
    p.close();
    QStringList list = result.split(" ");
    result = list.at(1);
    return result;
}

void UserData::startApp(){
    QNetworkRequest request;
    request.setUrl(QUrl("http://daka.mumetech.com/api/kdenlive/app?id=" + clientId + "&ver=" + limitVer));
    QNetworkReply *reply = m_networkManager->get(request);
    connect(reply, &QNetworkReply::finished, this, &UserData::finished);
}
void UserData::postUseData(QString objectName,QString action){
    if(action.isEmpty()){
        return;
    }
    if(clientId.isEmpty()){
        return;
    }
    QNetworkRequest request;
    request.setUrl(QUrl(QString("http://daka.mumetech.com/api/kdenlive/app/log?id=%1&ver=%2&sender=%3&action=%4").arg(clientId,limitVer,objectName,action)));
    //QNetworkAccessManager * networkAccessManager = new QNetworkAccessManager();
    QNetworkReply *replyLog = m_networkManager->get(request);
    connect(replyLog, &QNetworkReply::finished, this, &UserData::finishedLog);   
    
}
void UserData::finishedLog(){
    QNetworkReply *replyLog = static_cast<QNetworkReply*>(sender());
    if(replyLog->error() != QNetworkReply::NoError) {
        replyLog->deleteLater();
        return;
    }
}
void UserData::finished(){
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(reply->error() != QNetworkReply::NoError) {
        reply->deleteLater();
        return;
    }
    QByteArray bytes = reply->readAll();
    reply->deleteLater();
    QJsonDocument res = QJsonDocument::fromJson(bytes);
    QJsonObject jsonObj = res.object();
    int days = jsonObj["days"].toInt();
    if(days != -1){
        KMessageBox::information(this, QString("您的剩余试用天数为%1天，试用结束后部分功能将不能正常使用。").arg(days));
        if(days==0){
            postUseData("试用结束","退出应用");
            QApplication::exit(0);
        }
    }
}