#pragma once

#include <QString>
#include <QNetworkAccessManager>
#include <QWidget>
namespace Mlt {
}
class UserData : public QWidget
{
    Q_OBJECT

    public:
        void startApp();
        //void exitApp();
        void postUseData(QString objectName,QString action);
        UserData(QWidget *parent = Q_NULLPTR);

    private:
        QString getCsproductId();
        QNetworkAccessManager *m_networkManager;
        QString clientId;
        QString limitVer;
        void finished();
        void finishedLog();
};